'''
  Final project for Udacity - Full Stack foundations

'''
# Imports
# # Flask
from flask import (Flask, render_template, request, redirect,
                   url_for, flash, jsonify)
# # DB
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from database_setup import Base, Restaurant, MenuItem

# # Open DB
engine = create_engine('sqlite:///restaurantmenu.db')
Base.metadata.bind = engine

DBSession = sessionmaker(bind=engine)
session = DBSession()

# defome app
app = Flask(__name__)

app_title = 'Restaurant list'


# Routes
@app.route('/')
@app.route('/restaurants/')
def restaurant_list():
    '''List all restaurants'''

    restaurants = session.query(Restaurant).all()

    return render_template('restaurantlist.html',
                           restaurants=restaurants,
                           app_title=app_title)


@app.route('/restaurant/new/', methods=['GET', 'POST'])
def restaurant_new():
    '''Add new restaurant'''

    if request.method == 'POST':
        # Add new restaurant
        if request.form['submit'] == 'Add':
            new_restaurant = Restaurant(name=request.form['name'])
            session.add(new_restaurant)
            session.commit()
            flash('New restaurant created')

        # Redirect to restaurant list on cancel and create
        elif request.form['submit'] == 'Cancel':
            return redirect(url_for('restaurant_list'))

        return redirect(url_for('restaurant_list'))

    else:
        return render_template('restaurantnew.html',
                               app_title=app_title)


@app.route('/restaurant/<int:restaurant_id>/edit/',
           methods=['GET', 'POST'])
def restaurant_edit(restaurant_id):
    '''Edit restaurant'''

    restaurant = session.query(Restaurant)
    restaurant = restaurant.filter_by(id=restaurant_id).one()

    if request.method == 'POST':
        # Save info
        if request.form['submit'] == 'Save':
            _name = request.form['name']

            if _name:
                restaurant.name = _name

            session.add(restaurant)
            session.commit()
            flash('Restaurant information changed')

        # Redirect to list on cancel and change
        elif request.form['submit'] == 'Cancel':
            return redirect(url_for('restaurant_list'))

        return redirect(url_for('restaurant_list'))

    else:
        return render_template('restaurantedit.html',
                               app_title=app_title,
                               restaurant=restaurant,
                               restaurant_id=restaurant_id)


@app.route('/restaurant/<int:restaurant_id>/delete/',
           methods=['GET', 'POST'])
def restaurant_delete(restaurant_id):
    '''Delete restaurant'''

    restaurant = session.query(Restaurant)
    restaurant = restaurant.filter_by(id=restaurant_id).one()

    if request.method == 'POST':
        # Delete restaurant
        if request.form['submit'] == 'Delete':
            session.delete(restaurant)
            session.commit()
            flash('Restaurant information deleted')

        # Show list on cancel and delete
        elif request.form['submit'] == 'Cancel':
            return redirect(url_for('restaurant_list'))

        return redirect(url_for('restaurant_list'))

    else:
        return render_template('restaurantdelete.html',
                               app_title=app_title,
                               restaurant=restaurant,
                               restaurant_id=restaurant_id)


@app.route('/restaurant/<int:restaurant_id>/menu/')
def restaurant_menu(restaurant_id):
    '''Show restaurant menu'''

    restaurant = session.query(Restaurant)
    restaurant = restaurant.filter_by(id=restaurant_id).one()
    menu_items = session.query(MenuItem)
    menu_items = menu_items.filter_by(restaurant_id=restaurant.id).all()

    return render_template('menulist.html', app_title=app_title,
                           restaurant=restaurant,
                           menu_items=menu_items)


@app.route('/restaurant/<int:restaurant_id>/menu/new/',
           methods=['GET', 'POST'])
def menu_item_new(restaurant_id):
    '''Add new menu item to restaurant'''

    restaurant = session.query(Restaurant)
    restaurant = restaurant.filter_by(id=restaurant_id).one()

    # Add menu item
    if request.method == 'POST':

        if request.form['submit'] == 'Add':
            new_menu_item = MenuItem(name=request.form['name'],
                                     restaurant_id=restaurant_id,
                                     description=request.form['description'],
                                     price=request.form['price'])

            session.add(new_menu_item)
            session.commit()
            flash('New menu item created')

        # Show full menu after cancel and add
        elif request.form['submit'] == 'Cancel':
            return redirect(url_for('restaurant_menu',
                                    restaurant_id=restaurant_id))

        return redirect(url_for('restaurant_menu',
                                restaurant_id=restaurant_id))

    else:
        return render_template('menunew.html', app_title=app_title,
                               restaurant=restaurant)


@app.route('/restaurant/<int:restaurant_id>/menu/<int:menu_item_id>/edit/',
           methods=['GET', 'POST'])
def menu_item_edit(restaurant_id, menu_item_id):
    '''Edit restaurant menu item'''

    restaurant = session.query(Restaurant)
    restaurant = restaurant.filter_by(id=restaurant_id).one()
    menu_item = session.query(MenuItem)
    menu_item = menu_item.filter_by(id=menu_item_id).one()

    if request.method == 'POST':

        if request.form['submit'] == 'Save':
            _name = request.form['name']
            _description = request.form['description']
            _price = request.form['price']

            if _name:
                menu_item.name = _name
            if _description:
                menu_item.description = _description
            if _price:
                menu_item.price = _price

            session.add(menu_item)
            session.commit()
            flash('Menu information changed')

        elif request.form['submit'] == 'Cancel':
            return redirect(url_for('restaurant_menu',
                                    restaurant_id=restaurant_id))

        return redirect(url_for('restaurant_menu',
                                restaurant_id=restaurant_id))

    else:
        return render_template('menuedit.html', app_title=app_title,
                               restaurant=restaurant,
                               menu_item=menu_item)


@app.route('/restaurant/<int:restaurant_id>/menu/<int:menu_item_id>/delete/',
           methods=['GET', 'POST'])
def menu_item_delete(restaurant_id, menu_item_id):
    '''Delete restaurant menu item'''

    restaurant = session.query(Restaurant)
    restaurant = restaurant.filter_by(id=restaurant_id).one()
    menu_item = session.query(MenuItem)
    menu_item = menu_item.filter_by(id=menu_item_id).one()

    if request.method == 'POST':
        # Delete item
        if request.form['submit'] == 'Delete':
            session.delete(menu_item)
            session.commit()
            flash('Menu item deleted')

        # Show full menu on cancel and delete
        elif request.form['submit'] == 'Cancel':
            return redirect(url_for('restaurant_menu',
                                    restaurant_id=restaurant_id))

        return redirect(url_for('restaurant_menu',
                                restaurant_id=restaurant_id))

    else:
        return render_template('menudelete.html',
                               restaurant=restaurant,
                               menu_item=menu_item,
                               app_title=app_title)


# API Routes
@app.route('/restaurants/json')
def restaurant_list_json():
    '''Return restaurant list'''

    restaurants = session.query(Restaurant).all()

    return jsonify(restaurants=[restaurant.serialize for restaurant in
                   restaurants])


@app.route('/restaurant/<int:restaurant_id>/menu/json')
def restaurant_menu_json(restaurant_id):
    '''Return restaurant menu'''

    items = session.query(MenuItem)
    items = items.filter_by(restaurant_id=restaurant_id).all()

    return jsonify(MenuItems=[item.serialize for item in items])


@app.route('/restaurant/<int:restaurant_id>/menu/<int:menu_id>/json')
def menu_item_json(restaurant_id, menu_id):
    '''Return menu item information'''

    item = session.query(MenuItem).filter_by(id=menu_id).one()

    return jsonify(MenuItem=item.serialize)


if __name__ == '__main__':
    app.secret_key = 'sekrit'
    app.debug = True
    app.run(host='0.0.0.0', port=5000)
